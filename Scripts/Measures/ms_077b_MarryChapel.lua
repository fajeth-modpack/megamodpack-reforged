--[[ Code Rework.

Changelog:
- Fixed multiple bugs & optimised overall code.
- Created cutscene event for ceremonies.
- Added guests' reactions based on personality & favour.

Bugs fixed:
- Unlike previous dev. version, wedding ceremonies won't cause game crash when saving or loading anymore.
- Fixed an issue where Sims would never sit down on a given bench.

TODO: Royal Weddings;
TODO: Rhetoric challenge;
TODO: EventSchedules.

Preparations.

	[Royal Weddings]

	The entire country is made aware of the wedding to come (shall the Sim be considered Royal) with a MsgNews. Town Criers will spread the news in immersive in-game dialogs throughout the different town centers. Special NPCs will spawn at the town which is considered to be holding the Royal's main residence (Royal Guards) and will be protecting the Residence, the Royal and their soon-to-be spouse/husband.

	Both the Royal and the spouse/husband will be able to interact with one another in order to get ready for the ceremony, most especially the Rhetoric challenge. Three choices are available based on money spendings (from the cheapest to the most expensive):
	- train / prepare speech without help at the main residence;
	- train / prepare speech with a famous writer who is commissioned for this very event;
	- train / prepare speech at the court of some nobles' in a foreign Kingdom (Sims temporarily leave the map).

	Any of these three choices will be situational, and will vary according to the Sims' skills and reputation. For example, the first choice may be convenient if the Sims' skills in rhetoric are high enough. On the other hand, the third choice may be valuable to gain favours from another country.

	Note: a Royal can be assigned a fiancé(e) from abroad and / or get married to a foreign Royal with a special action measure. 

	In the case of a Royal assigning a foreign Royal fiancé(e) to their own children, the Royal adult will be introduced to several candidates and will have to choose one of them. Both fiancés will be required to spend time together in both respective kingdoms and will require private tutorship as a special education, until they reach adulthood and can get married.

	In the case of an adult Royal seeking mariage with another adult foreign Royal, the process will be similar to the exception of the tutorship and time spent together. The Royal seeking mariage may chose to purposedly not meet their soon-to-be spouse / husband, which would be an option for a political wedding. Should the Royal seek true love, they may chose to meet the new "candidate" beforehand.

	Rhetoric challenge: Royals will be asked to deliver a speech to the people and may chose to do so in different locations:
	- during the wedding ceremony at the chapel: the speech will have a limited audience, which may upset the commoners and satisfy the wealthiest;
	- upon exiting the chapel: which will have no favour modifier to either the commoners nor the wealthiest;
	- at the town's market: this will grant a positive favour modifier to the commoners;
	- in the town hall's meeting room: positive bonus for all holders of political offices and wealthy sims attending.

	The town criers reportedly give updates on the ceremony. Certain actions may modify the favour of a large group of sims, should they hear about it.
]]