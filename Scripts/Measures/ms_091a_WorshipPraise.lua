function Run()
	if not AliasExists("Destination") then
		StopMeasure()
	end
	local MeasureID = GetCurrentMeasureID("")
	local TimeOut = mdata_GetTimeOut(MeasureID)
	SetRepeatTimer("", GetMeasureRepeatName2("WorshipPraise"), TimeOut)
	SetRepeatTimer("", GetMeasureRepeatName2("WorshipScold"), TimeOut)
	
	if (GetInsideBuilding("", "church")) then
		if GetImpactValue("church","MassInProgress")==0 then
			AddImpact("church","MassInProgress",1,1)
		end
		SetProperty("church", "MassInProgress", GetID(""))
		MsgSay("","@L_CHURCH_091_PREPAREWORSHIP_WORSHIPPING_PRAISE_INTRO")
		PlayAnimationNoWait("","preach")
		MsgSay("","@L_CHURCH_091_PREPAREWORSHIP_WORSHIPPING_PRAISE_MAINPART",GetID("Destination"))
		BuildingGetOwner("church","MrChurch")
		BuildingGetInsideSimList("church","sims_in_church")
		ListRemove("sims_in_church", "")
		ListRemove("sims_in_church", "MrChurch")
		feedback_MessageCharacter("Destination","@L_CHURCH_091_PREPAREWORSHIP_PRAISE_HEAD_+0",
							"@L_CHURCH_091_PREPAREWORSHIP_PRAISE_BODY_+0",GetID("Destination"),GetID("church"),GetID("MrChurch"))
		
		local PreacherSkill = GetSkillValue("", RHETORIC)
		for i=0,ListSize("sims_in_church")-1, 1 do
			ListGetElement("sims_in_church", i, "receiver")
			
			if GetID("Destination") ~= GetID("receiver") then
				chr_ModifyFavor("receiver", "Destination", (GL_FAVOR_MOD_TINY+PreacherSkill))
			end
		end
		chr_ModifyFavor("destination", "MrChurch", (GL_FAVOR_MOD_TINY+PreacherSkill))
	end
end

function CleanUp()
	StopAnimation("")
	if GetID("church")~=-1 then
		RemoveProperty("church", "MassInProgress")
		RemoveImpact("church", "MassInProgress")
	end
	MeasureRun("", 0, "PrepareWorship", true)
end

function GetOSHData(MeasureID)
	--can be used again in:
	OSHSetMeasureRepeat("@L_ONSCREENHELP_7_MEASURES_TIMEINFOS_+2",Gametime2Total(mdata_GetTimeOut(MeasureID)))
end

