
SETLOCAL
@echo off 

IF NOT EXIST GuildII.exe (
	cd ..
	IF NOT EXIST GuildII.exe (
		echo Could not find GuildII.exe in this folder or parent folder. Abort.
		pause
		exit
	)
	echo Found GuildII.exe in parent folder.
)

echo Update mod ...
git pull

echo ++++++++++++++++++++++++++++++++
echo -------Available languages:-----
for /D %%d in (Translations/*) do echo %%d
echo --------------SELECT-------------
SET /P language=Choose your language: 
echo Copy translation ...
robocopy Translations/%language% . /is /it /im /e /xf .gitkeep /xf *.txt /log+:reforged-installer.log

echo Update done. Enjoy the Reforged!
ENDLOCAL
pause


